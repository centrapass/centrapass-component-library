import './App.css';
import {CentraPassProvider, LiveSelfie} from "@centrapass/component-library";

const Loading = () => {
    return (
        <div>
            Loading
        </div>
    )
}

function App() {
  return (
    <div className="App">
        <CentraPassProvider theme={{
            primaryColor: "blue",
            button: {
                primary: {
                  color: "white"
                },
                borderRadius: 10
            }
        }}>
            <div style={{width: 700}}>
                <LiveSelfie height={300} onComplete={(value)=>console.log(value)} loadingComponent={Loading} onChangeStep={(step)=>console.log(step)}/>
            </div>
        </CentraPassProvider>
    </div>
  );
}

export default App;
