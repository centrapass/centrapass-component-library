const path = require('path')
const webpack = require('webpack')

module.exports = {
    entry: './src/index.ts',
    mode: "production",
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        library: {
            name: 'component-library',
            type: 'umd'
        }
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                loader: 'ts-loader'
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
                sideEffects: true
            },
        ]
    },
    externals: {
        "react": "react",
        "react-dom": "react-dom",
    }
}