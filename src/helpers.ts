export const isMobileDevice = () => {
    return (
        typeof window.orientation !== 'undefined' ||
        navigator.userAgent.indexOf('IEMobile') !== -1
    );
};

export const jsToCssSize = (value: string | number) => {
    if (typeof value === "string"){
        return value
    }
    return value + 'px'
}