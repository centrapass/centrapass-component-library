import React, {SyntheticEvent, useContext, useMemo} from 'react';
import CentraPassContext from "../../context/CentraPassContext";
import {themeButtonConfigResolver} from "../../context/helper";

type ButtonProps = {
    /**
     * Is this the principal call to action on the page?
     */
    primary?: boolean;

    /**
     * How large should the button be? Contain takes the size of the parent container
     */
    size?: 'small' | 'medium' | 'large' | 'contain';

    color?: string

    /**
     * Button contents
     */
    children: React.ReactNode;

    /**
     * Override Button styles
     */
    style?: React.CSSProperties

    /**
     * Optional click handler
     */
    onClick?: (event: SyntheticEvent) => void;
}

/**
 * Primary UI component for user interaction
 */
const Button = ({
    primary = false,
    size = 'medium',
    style = {},
    children,
    color,
    onClick
}: ButtonProps) => {
    const { theme } = useContext(CentraPassContext)
    let computedStyle = useMemo(()=>{
        /* if not explicitly stated through props pull styling from theme properties */
        if (!color) color = primary ? theme.primaryColor : theme.secondaryColor

        let width, height;
        switch(size){
            case "small":
                width = 150
                height = 40
                break
            case "medium":
                width = 200
                height = 60
                break
            case "large":
                width = 250
                height = 80
                break
            case "contain":
                width = "100%"
                height = 60
                break;
            default:
                width = "100%"
                height = 60
                break;
        }
        let computedStyle = {}
        if(!theme.meta.button.noComputedStyle){
            const button = themeButtonConfigResolver(theme.button, primary ? "primary" : "secondary")

            // Color property overrides
            const backgroundColor = primary ? color || button.backgroundColor : "transparent"
            const borderColor = primary ? undefined : color || button.borderColor

            computedStyle = {
                width,
                height,
                ...button,
                fontSize: height * 0.3,
                backgroundColor,
                borderColor,
            }
        }
        return computedStyle
    }, [theme.meta.button.noComputedStyle])


    return (
        <button
            style={{
                ...computedStyle,
                ...style // Override all other styles
            }}
            className={`button-component ${primary && "primary"}`}
            onClick={onClick}
        >
            {children}
        </button>
    );
};

export default Button
