import React, {useContext} from 'react';
import './styles.css'
import CentraPassContext from "../../context/CentraPassContext";

type Props = {
    steps: number;
    currentStep: number;
    fontColor?: string;
    color?: string
};

const NumberStep: React.FC<{ color?: string, size: number, stepNumber: number, fontColor?: string }> = (
    {
        color,
        size,
        stepNumber,
        fontColor
    }
) => (
    <div style={{
        minWidth: size,
        height: size,
        borderColor: color,
        color: fontColor,
        fontFamily: "Verdana",
        fontSize: 12,
        backgroundColor: "white",
        borderStyle: "solid",
        borderWidth: 2,
        borderRadius: "50%",
        boxSizing: "border-box",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        overflow: "hidden"
    }}
         className="progress-step-outer-circle"
    >
        <div style={{
            backgroundColor: color,
            minWidth: size,
            height: size,
            borderRadius: "50%",
            opacity: 0.3,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            position: "absolute",
        }}
             className="progress-step-inner-circle"
        />
        <div style={{position: "relative"}} className="progress-step-number">
            {stepNumber}
        </div>
    </div>
)


const CurrentStep: React.FC<{ color?: string, size: number }> = ({color, size}) => (
    <div style={{
        minWidth: size,
        height: size,
        borderColor: color,
        borderStyle: "solid",
        borderWidth: 2,
        borderRadius: "50%",
        boxSizing: "border-box",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }}
         className="progress-step-outer-circle"
    >
        <div style={{
            backgroundColor: color,
            minWidth: size * 0.6,
            height: size * 0.6,
            borderRadius: "50%",
        }}
             className="progress-step-inner-circle"
        />
    </div>
)

const CompletedStep: React.FC<{ color?: string, size: number }> = ({color, size}) => (
    <div style={{
        minWidth: size,
        height: size,
        backgroundColor: color,
        borderRadius: "50%",
        boxSizing: "border-box",
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }}
         className="progress-step-outer-circle"
    >
        <div style={{
            backgroundColor: color,
            minWidth: size * 0.6,
            height: size * 0.6,
            borderRadius: "50%",
        }}
             className="progress-step-inner-circle"
        />
    </div>
)

const ProgressBar: React.FunctionComponent<Props> = (
    {
        steps,
        currentStep,
        fontColor,
        color
    }
) => {
    const {theme} = useContext(CentraPassContext)
    /* if not explicitly stated through props pull styling from theme properties */
    if (!color) color = theme.primaryColor
    if (!fontColor) fontColor = "gray"

    const renderStep = (stepNumber: number) => {
        let step;

        if (stepNumber < currentStep) {
            step = <div className="progress-step completed"/>
            step = <CompletedStep color={color} size={30}/>
        } else if (currentStep == stepNumber) {
            step = <CurrentStep color={color} size={30}/>
        } else {
            step = <NumberStep color={color} size={30} stepNumber={stepNumber + 1} fontColor={fontColor} />
        }

        return (
            <div className="progress-step-container" key={stepNumber}>
                {step}
                <div className="progress-line"/>
            </div>
        );
    }

    return (
        <div className="progress-bar">
            {[...Array(steps).keys()].map((_step, i) => {
                return renderStep(i)
            })}
        </div>
    );
};

export default ProgressBar;
