import React from 'react';
import Div100vh from 'react-div-100vh';
import './styles.css'

type Props = {
  type:
      | 'facematch'
      | 'id'
      | 'address'
      | 'email'
      | 'license'
      | 'passport'
      | 'phone'
      | 'liveliness';
  title: string;
  subTitle: string;
  documentType?: 'passport' | 'driving-license' | 'bill';
  buttonText?: string;
  onPress?: () => void;
  onImageCapture?: (base64Image: string) => void;
  verify?: (base64Image: string) => void;
};

const Index: React.FunctionComponent<Props> = props => {
  return (
      <div className="center-xs">
        <Div100vh>
          <div className="page-wrap">
            <div>
              <img
                  className="induction-image"
                  src={`/images/icons/${props.type}.svg`}
                  alt="induction-image"
              />
              <div className="induction-content">
                <h1 className="text-center induction-page-title">
                  {props.title}
                </h1>
                <p>{props.subTitle}</p>
              </div>
            </div>
          </div>
        </Div100vh>
      </div>
  );
};

export default Index;
