import React, {CSSProperties} from "react";

export interface ThemeType {
    primaryColor: string
    secondaryColor: string
    button: ThemeButton
    liveSelfie?: ThemeLiveSelfie
    meta: Meta
}

export interface Meta {
    button: {
        noComputedStyle?: boolean
    }
}

export interface ThemeLiveSelfie {
    inactiveColor?: string
    activeColor?: string
    trackingColor?: string
    trackingDotSize?: number
}

export interface ThemeButton extends CSSProperties {
    primary?: CSSProperties
    secondary?: CSSProperties
}

export interface  CentraPassContextType {
    theme: ThemeType
}

export const themeDefaultValue: ThemeType = {
    primaryColor: "#6AE05F",
    secondaryColor: "#315346",
    button: {
        primary: {
            borderRadius: 50,
            borderWidth: 2,
            borderStyle: "none",
            color: "white"
        },
        secondary: {
            borderStyle: "solid",
            color: "black",
            backgroundColor: "transparent"
        },
        borderRadius: 50,
        borderWidth: 2
    },
    meta: {
        button: {
            noComputedStyle: false
        }
    }
}

const CentraPassContext = React.createContext<CentraPassContextType>({
    theme: themeDefaultValue
})

export default CentraPassContext
