import React, {useContext} from "react";
import CentraPassContext, {ThemeType} from "./CentraPassContext";

interface Props {
    theme?: Partial<ThemeType>,
    children: React.ReactNode
}

const CentraPassProvider = ({ children, theme: localTheme = {}}: Props) => {
    const outerConfig = useContext(CentraPassContext)

    const config = React.useMemo(() => {
        outerConfig.theme = {
            ...outerConfig.theme,
            ...localTheme
        }
        return outerConfig
    }, [localTheme, outerConfig]);

    return <CentraPassContext.Provider value={config}>{children}</CentraPassContext.Provider>;
}

export default CentraPassProvider;
