import {ThemeButton} from "./CentraPassContext";
import {CSSProperties} from "react";

export function themeButtonConfigResolver(themeButtonConfig: ThemeButton, style: "primary" | "secondary"): CSSProperties {
    const styleConfig = themeButtonConfig[style]
    const resolved = {
        ...themeButtonConfig,
        ...styleConfig
    }
    delete resolved.primary
    delete resolved.secondary
    return resolved
}