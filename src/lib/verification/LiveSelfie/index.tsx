import React, {CSSProperties} from 'react';
import * as facemesh from '@tensorflow-models/face-landmarks-detection';
import * as tf from '@tensorflow/tfjs-core';
import {BlinkDetector} from './detectors/blink-detector';
import {SmileDetector} from './detectors/smile-detector';
import {Video} from './video';
import {Button, InductionPage, Loader, ProgressBar} from '../../components';
import '@tensorflow/tfjs-backend-webgl'

import './styles.css'
import CentraPassContext, {CentraPassContextType} from "../../context/CentraPassContext";
import {FaceLandmarksDetector} from "@tensorflow-models/face-landmarks-detection/dist/types";


interface StatusDefinitionItem {
    title: string,
    subTitle: string,
}

interface ButtonProps {
    text: string,
    style?: CSSProperties,
    mobileStyle?: CSSProperties
}

interface Props {
    /**
     * Height of the selfie view
     */
    height: number;

    /**
     * Callback to be called on completion of the flow
     * @param selfieBase64
     */
    onComplete?: (selfieBase64: string) => void;

    /**
     * Callback to be called if the user exits the flow
     */
    onFailure?: () => void;

    /**
     * Callback to show which step the live selfie is on
     * @param step
     */
    onChangeStep?: (step: Status) => void;

    /**
     * Whether to show in mobile mode
     */
    mobileView: boolean

    /**
     * The Status definitions for each state
     */
    statusDefinition: {
        blink: StatusDefinitionItem,
        smile: StatusDefinitionItem,
        align: StatusDefinitionItem,
        still: StatusDefinitionItem,
        done: StatusDefinitionItem
    }

    confirmButton: ButtonProps

    retakeButton: ButtonProps

    /**
     * Whether to show the progress bar. Default: true
     */
    showProgressBar: boolean

    /**
     * Whether to show the status. Default: true
     */
    showStatus: boolean

    /**
     * Custom loading component
     */
    loadingComponent: React.ReactNode

    /**
     * Skip the confirmation screen and let the parent application handle retakes and confirmation. If true
     * the onComplete function will be called straight away
     */
    skipConfirmation?: boolean
}

type Status = 'align' | 'smile' | 'blink' | 'still' | 'done';
type Steps = Status[];
type State = {
    steps: Steps;
    currentStep: number;
    loading: boolean;
    loadingText: string;
    selfieBase64: string | null;
    showInduction: boolean;
};

let model: FaceLandmarksDetector,
    ctx: CanvasRenderingContext2D,
    videoWidth: number,
    videoHeight: number,
    video: HTMLVideoElement,
    canvas: HTMLCanvasElement,
    stream: MediaStream;

let blinkDetector: BlinkDetector,
    smileDetector: SmileDetector,
    stillTimeout: NodeJS.Timeout | null

let isSafari = navigator.vendor.match(/[Aa]+pple/g)?.length || 0 > 0;

const Loading = () => {
    return (
        <div className="loading-overlay">
            <Loader />
            <p>Preparing Camera</p>
        </div>
    )
}

class LiveSelfie extends React.Component<Props, State> {
    context: CentraPassContextType
    static contextType = CentraPassContext
    static defaultProps = {
        statusDefinition: {
            blink: {
                title: 'Blink',
                subTitle: 'Keep your head still and blink',
            },
            smile: {
                title: 'Smile',
                subTitle: 'Keep your head still and smile',
            },
            align: {
                title: 'Align your face',
                subTitle:
                    'Position your face in the circle below and keep the camera at eye level.',
            },
            still: {
                title: 'Relax your face',
                subTitle: "Keep your eyes open and don't smile.",
            },
            done: {
                title: 'All done',
                subTitle: "You've completed your live selfie",
            },
        },
        mobileView: false,
        showProgressBar: true,
        showStatus: true,
        loadingComponent: <Loading />,
        retakeButton: {
            style: {},
            mobileStyle: {}
        },
        confirmButton: {
            style: {},
            mobileStyle: {}
        }
    }

    constructor(props: Props) {
        super(props);
        this.state = {
            steps: [],
            currentStep: 0,
            loading: true,
            loadingText: 'Preparing camera',
            selfieBase64: '',
            showInduction: false,
        };
    }

    getMaxSteps = (min: number, max: number) => {
        return Math.random() * (max - min) + min;
    };

    generateSteps = () => {
        let maxSteps = this.getMaxSteps(1, 1);
        let smile = true;
        const steps: Steps = [];
        steps.push('align');

        while (maxSteps >= 0) {
            maxSteps--;
            if (smile) {
                steps.push('smile');
                smile = false;
            } else {
                steps.push('blink');
                smile = true;
            }
            steps.push('still');
        }
        steps.push('done');
        console.debug('generated steps');
        return this.setState({steps} as Pick<State, keyof State>);
    };

    init = async () => {
        console.debug('init called');
        await this.initCamera();
        console.debug('init Camera done');

        await this.startFaceDetection();
    };

    componentDidMount = () => {
        this.generateSteps();
        this.init()
    };

    closeInduction = () => {
        this.setState(
            {
                showInduction: false,
            } as Pick<State, keyof State>,
            this.init,
        );
    };

    initCamera = async () => {
        console.debug('initCamera called');
        video = document.getElementById('video')! as HTMLVideoElement
        video.setAttribute('autoplay', '');
        video.setAttribute('muted', '');
        video.setAttribute('playsinline', '');

        console.debug('initCamera video: ', video);

        try {
            stream = await navigator.mediaDevices.getUserMedia({
                audio: false,
                video: {
                    facingMode: 'user',
                    // width & height are swapped to get full screen video on mobile https://stackoverflow.com/a/62598616
                    width: this.props.mobileView ? this.props.height : undefined,
                    height: this.props.mobileView ? window.innerWidth : undefined,
                },
            });
        } catch (err) {
            console.debug(' getUserMedia error: ', err);
        }
        video.srcObject = stream;
        return new Promise(resolve => {
            video.onloadedmetadata = () => {
                resolve(video);
            };
        });
    };

    changeStep = (step: number) => {
        this.setState({currentStep: step} as Pick<State, keyof State>);
        if(this.props.onChangeStep) this.props.onChangeStep(this.state.steps[step])
    }

    onSmile = () => {
        if (stillTimeout) {
            return;
        }
        const {steps, currentStep} = this.state;
        if (steps[currentStep] === 'smile') {
            this.changeStep(this.state.currentStep + 1)
        } else {
            this.changeStep(this.state.currentStep - 1)
        }
    };

    onBlink = () => {
        if (stillTimeout) {
            return;
        }
        const {steps, currentStep} = this.state;
        if (steps[currentStep] === 'blink') {
            this.changeStep(this.state.currentStep + 1)
        } else {
            this.changeStep(this.state.currentStep - 1)
        }
    };

    startFaceDetection = async () => {
        const { theme } = this.context
        console.debug('startFaceDetection called');
        video?.play();
        console.debug('startFaceDetection called video played');
        videoWidth = video?.videoWidth;
        videoHeight = this.props.mobileView ? this.props.height : video?.videoHeight;
        video.width = videoWidth;
        video.height = videoHeight;
        canvas = document.getElementById('output') as HTMLCanvasElement;
        canvas.width = videoWidth;
        canvas.height = videoHeight;

        ctx = canvas.getContext('2d')!;
        ctx.translate(canvas.width, 0);
        ctx.scale(-1, 1);
        ctx.fillStyle = theme.liveSelfie?.trackingColor || theme.primaryColor;
        ctx.strokeStyle = theme.liveSelfie?.trackingColor || theme.primaryColor;
        ctx.lineWidth = 0.9;
        await tf.setBackend('webgl');
        console.debug('startFaceDetection set backend completed');
        model = await facemesh.load();
        console.debug('startFaceDetection facemesh loaded');
        blinkDetector = new BlinkDetector({
            onBlink: this.onBlink,
            onMove: this.onMove,
            movementTrackerLimit: 3,
            eyeTrackerLimit: 3,
        });
        console.debug("blink detector loaded")

        smileDetector = new SmileDetector({
            onSmile: this.onSmile,
        });
        console.debug("smile detector loaded")

        this.setState({loading: false} as Pick<State, keyof State>);
        await this.detectFrames();
    };

    onMove = () => {
        // TODO: use this to show face moved UI. Ask user to keep steady face.
    };

    getSnapshot = async () => {
        // TODO: do snapshot need still image with no smile or blink ?
        let snapCanvas = document.createElement('canvas');
        snapCanvas.width = videoWidth;
        snapCanvas.height = videoHeight;
        let snapCanvasCtx = snapCanvas.getContext('2d')!;
        snapCanvasCtx.fillRect(0, 0, videoWidth, videoHeight);
        snapCanvasCtx.drawImage(video as CanvasImageSource, 0, 0, videoWidth, videoHeight);
        this.setState({
            selfieBase64: snapCanvas.toDataURL('image/png', 1.0)
        } as Pick<State, keyof State>);
        if(this.props.skipConfirmation){
            await this.handleCompletion()
        }
    };

    detectFrames = async () => {
        const {currentStep, steps} = this.state;
        if (steps[currentStep] === 'still') {
            if (!stillTimeout) {
                stillTimeout = setTimeout(() => {
                    this.changeStep(this.state.currentStep + 1)
                    stillTimeout = null;
                }, 3000);
            }
        }
        if (steps[currentStep] === 'done') {
            await this.getSnapshot();
            return;
        }
        // const estimatedFaces = await model.estimateFaces(video);
        const estimatedFaces = await model.estimateFaces({input: document.querySelector("video") as HTMLVideoElement});
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        const facialLandmarks = estimatedFaces[0] && estimatedFaces[0].scaledMesh;

        if (steps[currentStep] !== 'align' || steps[currentStep] !== 'done') {
            smileDetector.detect(facialLandmarks);
            blinkDetector.detect(facialLandmarks);
        }

        this.drawFaceMask(facialLandmarks);
        this.drawFacialLandmarks(facialLandmarks);

        requestAnimationFrame(this.detectFrames);
    };

    drawFaceMask = (facialLandmarks: any[] | tf.Tensor2D) => {
        const {theme} = this.context
        if (!facialLandmarks) {
            return;
        }
        const xRadius = videoWidth / (this.props.mobileView ? 3 : 4);
        const yRadius = videoHeight / (this.props.mobileView ? 3.2 : 2.2);

        const {currentStep} = this.state;
        const faceLeft = facialLandmarks && facialLandmarks[234];
        const faceRight = facialLandmarks && facialLandmarks[454];
        let outColor;
        if (
            faceLeft &&
            faceLeft[0] > videoWidth / 2 - xRadius &&
            faceRight &&
            faceRight[0] < videoWidth / 2 + xRadius
        ) {
            outColor = theme.liveSelfie?.activeColor || theme.primaryColor;
            if (currentStep === 0) {
                this.changeStep(1)
            }
        } else {
            outColor = theme.liveSelfie?.inactiveColor || theme.secondaryColor;
            if (currentStep !== 0) {
                this.changeStep(0)
            }
        }

        const detectorCanvas = document.createElement('canvas');
        detectorCanvas.width = videoWidth + 20;
        detectorCanvas.height = videoHeight + 20;
        const detectorCtx = detectorCanvas.getContext('2d')!;
        detectorCtx.strokeStyle = outColor;
        detectorCtx.lineWidth = 3;
        detectorCtx.ellipse(
            videoWidth / 2,
            videoHeight / 2,
            xRadius,
            yRadius,
            0,
            0,
            2 * Math.PI,
        );
        detectorCtx.stroke();

        const outerCanvas = document.createElement('canvas');
        outerCanvas.width = videoWidth;
        outerCanvas.height = videoHeight;
        const outerCtx = outerCanvas.getContext('2d')!;

        if (isSafari) {
            outerCtx.fillStyle = '#000a';
            outerCtx.fillRect(0, 0, videoWidth, videoHeight);
        } else {
            outerCtx.filter = this.props.mobileView ? 'brightness(50%)' : 'blur(4px)';
            outerCtx.drawImage(video as CanvasImageSource, 0, 0);
        }
        outerCtx.globalCompositeOperation = 'xor';
        outerCtx.ellipse(
            videoWidth / 2,
            videoHeight / 2,
            xRadius,
            yRadius,
            0,
            0,
            2 * Math.PI,
        );
        outerCtx.fill();
        ctx.drawImage(video as CanvasImageSource, 0, 0);
        ctx.drawImage(outerCanvas, 0, 0);
        ctx.drawImage(detectorCanvas, 0, 0);
    };

    drawFacialLandmarks = (facialLandmarks: any) => {
        if (!facialLandmarks) {
            return;
        }

        const {currentStep, steps} = this.state;
        let trackerPoints: number[] = [];
        switch (steps[currentStep]) {
            case 'blink':
                trackerPoints = [161, 160, 159, 158, 385, 386, 387, 388];
                break;
            case 'smile':
                trackerPoints = [
                    61,
                    185,
                    40,
                    39,
                    37,
                    0,
                    267,
                    269,
                    270,
                    409,
                    291,
                    375,
                    321,
                    405,
                    314,
                    17,
                    84,
                    181,
                    91,
                    146,
                ];
                break;
            default:
                trackerPoints = [];
        }

        for (let i = 0; i < facialLandmarks.length; i++) {
            const x = facialLandmarks[i][0];
            const y = facialLandmarks[i][1];

            if (trackerPoints.includes(i)) {
                ctx.beginPath();
                ctx.arc(x, y, this.context.theme.liveSelfie?.trackingDotSize || 1 /* radius */, 0, 2 * Math.PI);
                ctx.fill();
            }
        }
    };

    reTake = () => {
        this.changeStep(0)
        this.setState(
            {
                selfieBase64: null,
                steps: [],
            } as unknown as Pick<State, keyof State>,
            () => {
                this.generateSteps();
                this.init();
            },
        );
    };

    handleCompletion = async () => {
        stopCamera(stream);
        this.props.onComplete?.(this.state.selfieBase64!)
    };

    renderStatus = () => {
        const {steps, currentStep} = this.state;
        if (steps) {
            const current = steps[currentStep];
            console.debug(currentStep)
            const background =
                current === 'done'
                    ? 'linear-gradient(180deg, rgba(0,0,0,0.46122198879551823) 0%, rgba(0,0,0,0) 100%)'
                    : 'transparent';
            if (this.props.statusDefinition[current]) {
                return (
                    <div className="progress-instructions-wrapper">
                        <div
                            className={this.props.mobileView ? 'mobile-liveliness-status' : 'progress-instructions'}
                            style={this.props.mobileView ? {position: 'fixed', background} : {}}
                        >
                            <div className="progress-instructions-title">{`${this.props.statusDefinition[current].title}`}</div>
                            <div className="progress-instructions-subtitle">{`${this.props.statusDefinition[current].subTitle}`}</div>
                        </div>
                    </div>
                );
            }
        }
        return null
    };

    renderVideo = () => {
        const {selfieBase64} = this.state;
        return (
            <div className="camera-preview-area-wrapper">
                {selfieBase64 && (
                    <img
                        className="selfie"
                        src={selfieBase64}
                        style={
                            this.props.mobileView
                                ? {
                                    position: 'fixed',
                                    top: 0,
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    zIndex: -1,
                                    borderRadius: 0,
                                    backgroundColor: '#fff',
                                }
                                : {}
                        }
                        alt="selfie"
                    />
                )}
                <div
                    className={`${ this.props.mobileView && "mobile-video-container"} ${!!selfieBase64 && 'hide'}`}
                >
                    <Video height={this.props.height} mobileView={this.props.mobileView}/>
                </div>
            </div>
        )
    }

    render() {
        const isLandscape = window.innerHeight < window.innerWidth;
        if (this.state.showInduction) {
            return (
                <InductionPage
                    type="liveliness"
                    onPress={this.closeInduction}
                    title="Live Selfie"
                    subTitle="To prove you're a real person you'll need to take a live selfie."
                />
            );
        }
        const {selfieBase64} = this.state;
        return (
            <div style={{position: "relative"}} className="liveselfie-container">
                {this.props.mobileView && <div className="camera-background"/>}

                {this.state.loading && (
                    <div className="custom-loading-background">
                        {this.props.loadingComponent}
                    </div>
                )}

                {this.props.mobileView && isLandscape && (
                    <div className="loading-overlay">
                        <p>Landscape mode is not supported</p>
                    </div>
                )}
                <div className={this.props.mobileView ? undefined : 'page-wrap'}>
                    {
                        this.state.steps.length > 0 
                        && 
                        this.props.showProgressBar 
                        &&
                        !this.props.mobileView
                        &&
                        <ProgressBar steps={this.state.steps.length - 1} currentStep={this.state.currentStep} />
                    }
                    {this.props.showStatus && this.renderStatus()}
                    {this.renderVideo()}
                    <div
                        className={`button-container ${!(selfieBase64 && !this.props.skipConfirmation) && 'hide_footer'}`}
                        style={
                            this.props.mobileView
                                ? {
                                    flexDirection: 'column',
                                    position: 'fixed',
                                    padding: 0,
                                    bottom: 0,
                                    left: 0,
                                    right: 0,
                                    zIndex: 4
                                }
                                : {}
                        }
                    >
                        <div className="selfie-button">
                            <Button
                                onClick={() => this.reTake()}
                                size="contain"
                                style={
                                    this.props.mobileView
                                        ? {
                                            minWidth: '80%',
                                            maxWidth: '80%',
                                            height: 55,
                                            marginBottom: 10,
                                            zIndex: 500,
                                            justifyContent: "center",
                                            ...this.props.retakeButton.mobileStyle
                                        }
                                        : { ...this.props.retakeButton.style }
                                }
                            >
                                {this.props.retakeButton.text || "Retake"}
                            </Button>
                        </div>
                        <div className="selfie-button">
                            <Button
                                onClick={() => this.handleCompletion()}
                                primary
                                size="contain"
                                style={
                                    this.props.mobileView
                                        ? {
                                            minWidth: '80%',
                                            maxWidth: '80%',
                                            height: 55,
                                            zIndex: 500,
                                            ...this.props.confirmButton.mobileStyle
                                        }
                                        : { ...this.props.confirmButton.style }
                                }
                            >
                                {this.props.confirmButton.text || "Confirm"}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const stopCamera = (stream: MediaStream) => {
    stream.getTracks().forEach(function (track) {
        track.stop();
    });
};

export default LiveSelfie
