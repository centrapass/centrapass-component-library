import React from 'react';
function renderVideo({ height, mobileView}: {height: number | string, mobileView: boolean}) {
  console.log('Render Video');

  return (
    <div>
      <canvas
        id="output"
        className="video-canvas"
        style={
          mobileView
            ? {
                position: 'absolute',
                zIndex: 1,
                width: '100%',
                height: height,
                top: 0,
                left: 0,
                bottom: 0,
              }
            : {}
        }
      />
      <video
        id="video"
        className="webcam"
        style={
          mobileView
            ? {
                width: '100%',
                height: height,
                position: 'absolute',
                top: 0,
                left: 0,
              }
            : {}
        }
      />
    </div>
  );
}

export const Video = React.memo(renderVideo);
