import { Tensor2D } from '@tensorflow/tfjs-core';
import distance from 'euclidean-distance';

export class SmileDetector {
  private onSmile: () => void;
  private smileStart = false;

  constructor({onSmile}: { onSmile: () => void }) {
    this.onSmile = onSmile;
  }

  public detect(facialLandmarks: Tensor2D | number[][]) {
    if (!facialLandmarks) {
      return;
    }

    const eyeDistance = distance(facialLandmarks[133], facialLandmarks[263]);
    const smileDistance = distance(facialLandmarks[61], facialLandmarks[291]);
    const smileFactor = smileDistance / eyeDistance;

    if (smileFactor < 0.9 && !this.smileStart) {
      this.smileStart = true;
    }
    if (smileFactor > 1 && this.smileStart) {
      this.onSmile();
      this.smileStart = false;
    }
  }
}
