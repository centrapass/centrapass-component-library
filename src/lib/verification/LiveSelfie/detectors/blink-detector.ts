import { Tensor2D } from '@tensorflow/tfjs-core';
import distance from 'euclidean-distance';

export class BlinkDetector {
  private eyeMovementThreshold = 0.4;
  private noseMovementThreshold = 1;

  private eyeTrackerQue: Array<number> = [];
  private readonly eyeTrackerLimit;
  private movementTrackerQue: Array<[any, any]> = [];
  private readonly movementTrackerLimit;
  private blinkStart = false;
  private readonly onBlink;
  private blinkTimeout: NodeJS.Timeout;
  private readonly onMove;

  constructor({onBlink, onMove, eyeTrackerLimit = 0, movementTrackerLimit = 0}: {
    onBlink: () => void
    onMove: () => void
    eyeTrackerLimit: number
    movementTrackerLimit: number
  }) {
    this.onBlink = onBlink;
    this.onMove = onMove;
    this.eyeTrackerLimit = eyeTrackerLimit;
    this.movementTrackerLimit = movementTrackerLimit;
  }

  private isMovementDetected(latestPoint: any[]) {
    if (this.movementTrackerQue.length < this.movementTrackerLimit) {
      this.movementTrackerQue.push([latestPoint[0], latestPoint[1]]);
    } else {
      this.movementTrackerQue.shift();
    }

    const movedDistance = distance(
        [latestPoint[0], latestPoint[1]],
        this.getAverageMovement(),
    );
    return movedDistance > this.noseMovementThreshold;
  }

  public detect(facialLandmarks: Tensor2D | any[][]) {
    if (!facialLandmarks) {
      return;
    }

    // calculate average distance between 6 eyelid landmarks and nose tip.
    let avgNoseToEyeLidDistance =
      (distance(facialLandmarks[160], facialLandmarks[94]) +
        distance(facialLandmarks[159], facialLandmarks[94]) +
        distance(facialLandmarks[158], facialLandmarks[94]) +
        distance(facialLandmarks[385], facialLandmarks[94]) +
        distance(facialLandmarks[386], facialLandmarks[94]) +
        distance(facialLandmarks[387], facialLandmarks[94])) /
      6;

    // get noseTip landmark, used for movement detection.
    const noseTip = [facialLandmarks[94][0], facialLandmarks[94][1]];

    if (this.isMovementDetected(noseTip)) {
      this.onMove();
      return;
    }

    this.eyeTrackerQue.push(avgNoseToEyeLidDistance);

    if (this.eyeTrackerQue.length < this.eyeTrackerLimit) {
      return;
    } else {
      this.eyeTrackerQue.shift();
    }

    if (
      avgNoseToEyeLidDistance - this.getPreviousEyeDistanceAvg() >
        this.eyeMovementThreshold &&
      !this.blinkStart
    ) {
      this.blinkStart = true;
      clearTimeout(this.blinkTimeout);
      this.blinkTimeout = setTimeout(() => {
        this.blinkStart = false;
      }, 1000);
    }

    if (
      this.getPreviousEyeDistanceAvg() - avgNoseToEyeLidDistance >
        this.eyeMovementThreshold &&
      this.blinkStart
    ) {
      clearTimeout(this.blinkTimeout);
      this.onBlink && this.onBlink();
      this.blinkStart = false;
    }
  }

  private getPreviousEyeDistanceAvg() {
    return (
      this.eyeTrackerQue.reduce((a, b) => a + b) / this.eyeTrackerQue.length
    );
  }
  private getAverageMovement() {
    let totalX = 0,
      totalY = 0;
    this.movementTrackerQue.forEach(point => {
      totalX = totalX + point[0];
      totalY = totalY + point[1];
    });

    return [
      totalX / this.movementTrackerQue.length,
      totalY / this.movementTrackerQue.length,
    ];
  }
}
