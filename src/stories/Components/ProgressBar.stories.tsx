import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import {ProgressBar} from "../../";

export default {
  title: 'Components/Progress Bar',
  component: ProgressBar,
} as ComponentMeta<typeof ProgressBar>;

const Template: ComponentStory<typeof ProgressBar> = (args) => <ProgressBar {...args} />;

export const Default = Template.bind({});
Default.args = {
  steps: 5,
  currentStep: 2
};