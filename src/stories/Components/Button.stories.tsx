import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import {Button} from "../../";

export default {
  title: 'Components/Button',
  component: Button,
  argTypes: {
    children: {
      control: {
        type: 'text'
      }
    },
    color: {
      control: {
        type: 'color'
      }
    }
  }
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  children: 'Button',
};

export const Secondary = Template.bind({});
Secondary.args = {
  children: 'Button',
};

export const Large = Template.bind({});
Large.args = {
  size: 'large',
  children: 'Button',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  children: 'Button',
};

export const Contain = Template.bind({});
Contain.args = {
  size: 'contain',
  children: 'Button',
};
