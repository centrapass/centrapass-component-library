import React from "react";
import {ComponentMeta, ComponentStory} from "@storybook/react";
import {CentraPassProvider, LiveSelfie} from "../../";

export default {
    title: "Verification/Live Selfie",
    component: LiveSelfie,
}as ComponentMeta<typeof LiveSelfie>

const Template: ComponentStory<typeof LiveSelfie> = (args) => <LiveSelfie {...args} />

const CustomLoadingComponent = () => (
    <div>
        Loading
    </div>
)

export const Default = Template.bind({})
Default.args = {
    height: 600,
    statusDefinition: LiveSelfie.defaultProps.statusDefinition,
    onChangeStep: (step: string) => console.log(step),
    onComplete: (selfie: string) => { console.log(selfie) }
}

export const CustomLoading = Template.bind({})
CustomLoading.args = {
    height: 600,
    statusDefinition: LiveSelfie.defaultProps.statusDefinition,
    loadingComponent: <CustomLoadingComponent />,
    onComplete: (selfie: string) => { console.log(selfie) }
}

export const SkipConfirmation  = Template.bind({})
SkipConfirmation.args = {
    height: 600,
    statusDefinition: LiveSelfie.defaultProps.statusDefinition,
    loadingComponent: <CustomLoadingComponent />,
    onComplete: (selfie: string) => { console.log(selfie) },
    skipConfirmation: true
}

export const WithProvider = Template.bind({})
WithProvider.decorators = [
    (Story: any) => (
        <CentraPassProvider theme={{
            primaryColor: "blue"
        }}>
            {Story()}
        </CentraPassProvider>
    )
]
WithProvider.args = {
    onComplete: (selfie: string) => { console.log(selfie) }
}
