# CentraPass Component Library

Helper components to build CentraPass frontends

## Storybook
Can be used to try out the components and even generate code for certain configs. Install using `yarn` and then run storybook using `yarn storybook`

## Usage
### CentraPassContext

This context is used to set the configuration all CentraPass Components will use such as the primary colour and secondary colour and other project wide customisations such as Button component styles. It can be used as follows:
```tsx
<div>
    <CentraPassProvider theme={{
        primaryColor: "blue"
    }}>
        <LiveSelfie height={300} onComplete={(value)=>console.log(value)}/>
    </CentraPassProvider>
</div>
```

## Examples
Examples are in the `examples` folder and can be used as a starting point for your use of the library